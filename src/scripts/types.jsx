export default {
  REGISTRATION: Symbol('REGISTRATION'),
  LOGIN: Symbol('LOGIN'),
  PROFILE: Symbol('PROFILE')
}
