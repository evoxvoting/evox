import AccountActions from '../actions/account.actions.jsx';
import axios from 'axios';

function header(token) {
  return {headers: {'Authorization': 'Bearer ' + token}};
}

export default {
  performLogin: {
    remote(state, user) {
      return axios.post('/api/auth', {
        email: user.email,
        password: ' '
      });
    },

    //loading: AccountActions.loadingResults, // (optional)
    success: AccountActions.loggedIn,
    error: AccountActions.fail
  },

  getProfile: {
    remote(state, token) {
      return axios.get('/api/users/me', header(token));
    },

    //loading: AccountActions.loadingResults, // (optional)
    success: AccountActions.gotProfile,
    error: AccountActions.fail
  },

  saveCredentials: {
    remote(state, user) {
      return axios.post('/api/users/', {
        "name": user.name,
        "surname": user.surname,
        "token": user.token,
        "email": user.email
      });
    },
    success: AccountActions.saved,
    error: AccountActions.fail
  }
};
