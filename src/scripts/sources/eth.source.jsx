import EToken from './etoken-lib.abstraction.jsx';
import AccountActions from '../actions/account.actions.jsx';
import PetitionActions from '../actions/petition.actions.jsx';
import PanelActions from '../actions/panel.actions.jsx';

export default  {
  getBalance: {
    remote(state, user) {
      return EToken.getBalance(user);
    },
    success: PanelActions.gotBalance,
    error: AccountActions.fail
  },
  createEthAccount: {
    remote(state, user) {
      return EToken.createNewUserAccount(user.password);
    },
    success: AccountActions.created,
    error: AccountActions.fail
  },
  validateAccount: {
    remote(state, user) {
      return EToken.unlockUser({token: user.token, password: user.password});
    },
    success: AccountActions.validated,
    error: AccountActions.fail
  },
  sendExternalFunds: {
    remote(state, to, money){
      console.log('Transaction prepared for ' + to);
      return EToken.sendFunds(to, money)
    },
    success: PetitionActions.sendDone,
    error: PetitionActions.sendFail
  },
  createPoll: {
    remote(state, token, password){
      return EToken.createPoll(token, password, Object.assign({}, state.petition))
    },
    success: PetitionActions.sendDone,
    error: PetitionActions.sendFail
  },
  voteForPoll: {
    remote(state, token, password, pollAddress){
      console.log(token, password, pollAddress);
      return EToken.vote(token, password, pollAddress)
    },
    success: PetitionActions.voteDone,
    error: PetitionActions.voteFail
  }
};
