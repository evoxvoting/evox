import EToken from '../etoken-lib/provider.jsx';
import Config from '../config.jsx';
import Registry from './poll.data.jsx';

EToken.start(Config.eth);
const key = Config.eth.key;
const fromBlock = Registry.startBlock;
const toBlock = 'latest';
const gas = "1000000";

const Web3 = require('web3');
const web3Provider = Registry.rpc;
const web3 = new Web3();
web3.setProvider(new web3.providers.HttpProvider(web3Provider));
window.w3 = web3;
window.ET = EToken;
window.R = Registry;
const RegistryContract = EToken.web3.eth.contract(Registry.registryAbi).at(Registry.registryAddress);
const PollForDeploy = EToken.web3.eth.contract(Registry.pollAbi).getData({data: Registry.pollBytes});
const Community = web3.eth.contract(Registry.communityAbi).at(Registry.communityAddress);

window.Registry = RegistryContract;
window.Comm = Community;


export default {
  getBalance(user){
    console.log(web3.eth.getBalance(user.token));
    return new Promise((done, fail) => {
      EToken.web3.eth.getBalance(user.token, 'latest', (error, result) => {
        console.log(result);
        error ? fail(error) : done(result.toNumber())

      });
    });
  },
  getBlock(){
    return new Promise((done, fail) => {
      EToken.web3.eth.getBlock('latest', (error, result) => error ? fail(error) : done(result));
    });
  },
  /* Login  */
  unlockUser(user){
    EToken.setPassword(user.password);
    return new Promise((done, fail) => {
      EToken.storage.getAccountByAddress(user.token, (error, result) => error ? fail(error) : done(user))
    });
  },
  /* Login */

  /* Account Create */
  createNewUserAccount(password)  {
    EToken.setPrivateKey(key);
    return Promise.resolve(password)
      .then(this.beginAccountCreate)
      .then(this.awaitAccountCreate)
      .then(this.unlockOwner)
      .then(this.requestUserToCommunity)
      .then(this.awaitRequestUserToCommunity)
      .then(this.acceptUserToCommunity)
      .then(this.acceptUserToCommunity)
  },

  beginAccountCreate(password)  {
    return new Promise((done, fail)=> {
      EToken.createAccount(password, (error, result) => error ? fail(error) : done(result));
    });
  },


  awaitAccountCreate(prevResult)  {
    return new Promise((done, fail)=> {
      EToken.waitForTransaction(prevResult.transactionHash, (error, result) => error ? fail(error) : done(prevResult.address));
    });
  },

  requestUserToCommunity(token)  {
    return new Promise((done, fail)=> {
      Community.request.sendTransaction(token, '0x0', '0x0', {from: Registry.owner, gas}, (error, tx) => error ? fail(error) : done({tx, token}));
    });
  },

  awaitRequestUserToCommunity(prevResult)  {
    return new Promise((done, fail)=> {
      EToken.waitForTransaction(prevResult.tx, (error, result) => error ? fail(error) : done(prevResult.token));
    });
  },

  acceptUserToCommunity(token)  {
    return new Promise((done, fail)=> {
      Community.addMember.sendTransaction(token, {from: Registry.owner, gas}, (error, tx) => error ? fail(error) : done(token));
    });
  },
  /* Account Create */

  /* Voting */
  vote(from, password, address){
    return Promise.resolve({token: from, password, address})
      .then(this.unlockUser)
      .then(this.voteCasting)
      .then(this.awaitVoteCasting)
      .then(this.getVotingReceipt);
  },

  voteCasting(user){
    const Poll = EToken.web3.eth.contract(Registry.pollAbi).at(user.address);
    return new Promise((done, fail) => {
      Poll.cast([1], {from: user.token, gas}, (error, tx)=> {
        console.log(error);
        error ? fail(error) : done({tx, user})
      });
    });
  },

  awaitVoteCasting(prevResult){
    return new Promise((done, fail) => {
      EToken.waitForTransaction(prevResult.tx, (error, result)=> error ? fail(error) : done(prevResult));
    });
  },

  getVotingReceipt(prevResult){
    return new Promise((done, fail) => {
      EToken.web3.eth.getTransactionReceipt(prevResult.tx, (error, result)=> error ? fail(error) : done(prevResult));
    });
  },
  /* Voting */

  /* Payment */
  sendFunds(token, amount)  {
    return Promise.resolve({token, amount})
      .then(this.unlockOwner)
      .then(this.sendEth)
      .then(this.awaitSendEth)
      .then(this.getBalance);
  },

  unlockOwner(user){
    return new Promise((done, fail) => {
      web3.personal.unlockAccount(Registry.owner, Registry.password, 300, (error, unlocked)=> error || !unlocked ? fail(error) : done(user));
    });
  },

  sendEth(user){
    return new Promise((done, fail) => {
      web3.eth.sendTransaction({from: Registry.owner, to: user.token, value: user.amount}, (error, tx)=> error ? fail(error) : done({user, tx}));
    });
  },

  awaitSendEth(prevResult){
    return new Promise((done, fail) => {
      EToken.waitForTransaction(prevResult.tx, (error, result)=> error ? fail(error) : done(prevResult.user));
    });
  },

  getBalance(user){
    return new Promise((done, fail) => {
      web3.eth.getBalance(user.token, (error, result) => error ? fail(error) : done(user));
    });
  },

  sendInKeeperFunds(from, to, password, amount)  {
    return new Promise((done, fail) => {
      EToken.setPassword(password);
      EToken.web3.eth.sendTransaction({from: from, to: to, value: amount}, (error, tx) => error ? fail(error) : done(tx))
    });
  },
  /* Payment */

  /* Poll in details*/
  getPollIndex(address){
    const poll = {address};
    return new Promise((done, fail) => {
      RegistryContract.pollIndex(poll.address, (error, result) => error ? fail(error) : done({result, poll}));
    });
  },
  getPollName(last){
    const poll = last.poll;
    poll.index = last.result.toNumber();
    return new Promise((done, fail) => {
      RegistryContract.pollNames(poll.index, (error, result) => error ? fail(error) : done({result, poll}));
    });
  },

  getPollQuestion(last){
    const poll = last.poll;
    poll.name = last.result;
    return new Promise((done, fail) => {
      EToken.web3.eth.contract(Registry.pollAbi).at(poll.address).questions((error, result) => error ? fail(error) : done({result, poll}));
    });
  },

  getPollStartDate(last){
    const poll = last.poll;
    poll.questions = last.result;
    // poll.name = last.result;
    return new Promise((done, fail) => {
      EToken.web3.eth.contract(Registry.pollAbi).at(poll.address).start((error, result) => error ? fail(error) : done({result, poll}));
    });
  },
  getPollEndDate(last){
    const poll = last.poll;
    poll.startTime = last.result.toNumber() * 1000;
    return new Promise((done, fail) => {
      EToken.web3.eth.contract(Registry.pollAbi).at(poll.address).end((error, result) => error ? fail(error) : done({result, poll}));
    });
  },

  getPollResults(last){
    const poll = last.poll;
    poll.endTime = last.result.toNumber() * 1000;
    return new Promise((done, fail) => {
      EToken.web3.eth.contract(Registry.pollAbi).at(poll.address).checkResults.call((error, result) => {
        error ? fail(error) : done(Object.assign(poll, {count: EToken.web3.toBigNumber(result[1]).toNumber()}));
      });
    });
  },

  receivePollList(receiver)  {
    RegistryContract.allEvents({fromBlock, toBlock}, (error, result) =>
      !error && result.event === 'Poll' && Promise.resolve(result.args.poll)
        .then(this.getPollIndex)
        .then(this.getPollName)
        .then(this.getPollQuestion)
        .then(this.getPollStartDate)
        .then(this.getPollEndDate)
        .then(this.getPollResults)
        .then((petition)=> {
          petition.pollInstance = EToken.web3.eth.contract(Registry.pollAbi).at(petition.address);
          return petition;
        })
        .then(receiver)
        .catch(error => console.error(error))
    );
  },
  /* Poll in details*/
  /* Create Poll*/
  deployingPollContract(user){
    console.info('Deploing poll contract 2/7...');
    return new Promise((done, fail) => {
      const PollContract = EToken.web3.eth.contract(R.pollAbi);
      PollContract.new({from: user.token, data: R.pollBytes, gas: 3000000}, (error, contract) => {
        if (error) {
          fail(error);
        } else if (contract.address !== undefined) {
          done({poll: PollContract.at(contract.address), user});
        }
      });
      // EToken.web3.eth.sendTransaction({from: user.token, gas, data: PollForDeploy}, (error, tx) => error ? fail(error) : done({tx, user}));
    });
  },
  /*

   awaitDeployingPollContract(prevResult){
   console.info('Waiting for been processed poll contract 3/7...');
   return new Promise((done, fail) => {
   EToken.waitForTransaction(prevResult.tx, (error, tx) => error ? fail(error) : done(prevResult));
   });
   },
   getDeployingReceipt(prevResult){
   console.info('Getting receipt of poll deploying 4/7...');
   const user = prevResult.user;
   return new Promise((done, fail) => {
   EToken.web3.eth.getTransactionReceipt(prevResult.tx, (error, receipt) => error ? fail(error) : done({receipt, user}));
   });
   },
   */
  updatingPollContract(prevResult){
    console.info('Poll updating and params setting 5/7...');
    const user = prevResult.user;
    const poll = prevResult.poll;
    const from = user.token;
    const info = user.info;
    //const pollAddress = prevResult.receipt.contractAddress;
    //const PollContract = EToken.web3.eth.contract(Registry.pollAbi).at(pollAddress);
    //user.info.address = pollAddress;
    return new Promise((done, fail) => {
      poll.setParams(Math.floor(info.start / 1000), Math.floor(info.end / 1000), [1], [], info.text, {from, gas}, (error, result) => error ? fail(error) : done({poll, user}));
    });
  },
  /*

   awaitUpdatingPollContract(prevResult){
   console.info('Waiting for updates will have applied 6/7...');
   return new Promise((done, fail) => {
   EToken.waitForTransaction(prevResult.tx, (error, result) => error ? fail(error) : done(prevResult.user));
   });
   },
   */
  addingPoolToRegistry (prevResult) {
    const poll = prevResult.poll;
    const user = prevResult.user;
    console.info('Adding poll to the registry 7/7...');
    return new Promise((done, fail) => {
      RegistryContract.addPoll(poll.address, user.info.title, {from: user.token, gas}, (error, result) => error ? fail(error) : done(result));
      //RegistryContract.addPoll.sendTransaction(user.info.address, user.info.title, {from: user.token, gas});
    });
  },
  createPoll(token, password, info)  {
    console.info('Unlocking user accoung 1/7...');
    return Promise.resolve({token, password, info})
      .then(this.unlockUser)
      .then(this.deployingPollContract)
      // .then(this.awaitDeployingPollContract)
      //  .then(this.getDeployingReceipt)
      .then(this.updatingPollContract)
      //.then(this.awaitUpdatingPollContract)
      .then(this.addingPoolToRegistry);
  }
  /* Create Poll*/
}

/*var pollsToDelete = [0xa1174c882cb2cf72768343da7bb38796c81d2f62
 ,0x0cdc7fef8f8d0ee77360060930aada1263b26ff7
 ,0xfa8826004fae76d989acbbb2ba06baf23b2dbcea
 ,0x01e9ef13579456382c97b3c1c208c5298e7ce989
 ,0x28fd8c640177e28a90631267afd514901f377c14
 ,0x8ccb9306110147ac66f56bf093b328644fed3ca7
 ,0x5d119885a2f8ea6ac476d035b7af0146f8d2c089
 ,0x9d6c11401574b6c90401399eaa6ead3e40803619
 ,0xfec9aed1f30cd5b921e5e05952d330f3a5308f2d
 ,0xb3f38ee89d7e2cbca15b71ee44a7148f36620a96
 ,0x580834470a5bc83918566d1b4fac20471a135195
 ,0xfd25d8008ae6aa7299d547ed66135e634dbbe80f
 ,0x8e8f55748a79790d88b7c246a530951a7649e29a
 ,0x4b762afd5b45ac0901f2efd0a648a2f2813cebcd
 ,0x56a1e2650b28ca39b2905448af3f9f60fd0db701
 ,0xa7d988867b56ae1dae3d08cfb8dc2aa3409079a2
 ,0xffd98ef8072fad1404faf09a4d9d400b1bc6d865
 ,0x84c0285b3bdb79670a8ac66b9a9912faa408db0d
 ,0xed3f15c18a4020171070fd84143bac8c4d25ebcd
 ,0x006f74db28078a9b76e6bfe269b2363104fbcda6
 ,0xb57b4f95344d94a6002f571eb1b4c6547a05f284
 ,0x3a0d50dc5e0b86b6956f2f51b82b9e64d344fc3c
 ,0xfb0e24f6e77ce585eb67549a79e660d654a46b6b
 ,0xdabfc0ac35f08bf9aadc5ca2dd218cfbce964ebe]*/
