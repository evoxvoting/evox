import alt from '../alt.jsx';
export default alt.generateActions(
  'login',
  'loggedIn',
  'gotProfile',
  'validated',
  'createAccount',
  'saved',
  'created',
  'errorClear',
  'fail'
);
