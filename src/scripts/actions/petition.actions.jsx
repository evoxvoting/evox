import alt from '../alt.jsx';
export default alt.generateActions(
  'startFetchingPetitions',
  'doFilter',
  'setFilter',
  'makePetition',
  'sendDone',
  'sendFail',
  'sendMoney',
  'vote',
  'voteDone',
  'voteFail',
  'setTitle',
  'setText',
  'setStart',
  'handleOpen',
  'handleClose',
  'setEnd',
  'updateVotePermissions',
  'reducingBy'
);
