import alt from '../alt.jsx';
export default alt.generateActions(
  'logout',
  'showRegister',
  'showLogin',
  'showProfile',
  'startLoading',
  'stopLoading',
  'setName',
  'setSurname',
  'setEmail',
  'setPassword',
  'setProfile',
  'gotBalance'
);
