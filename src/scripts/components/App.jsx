import React from 'react';
import AppBar from 'material-ui/lib/app-bar';
import LeftNav from 'material-ui/lib/left-nav';

import Toolbar from 'material-ui/lib/toolbar/toolbar';
import SearchPanel from './Search/Search.jsx';
import AccountPanel from './Account/Account.jsx';
import TabsPanel from './Petitions/TabsContainer.jsx';


export default class Application extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.activateUserBar = this.activateUserBar.bind(this);
    this.activateAddBar = this.activateAddBar.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.tabChange = this.tabChange.bind(this);

    this.state = {
      open: false,
      add: false,
      tabIndex: 0,
      title: "Електронні петиції"
    };
  }

  render() {
    return (
      <div>
        <AppBar titleStyle={{fontWeight:'bold'}} title={this.state.title} onRightIconButtonTouchTap={this.activateAddBar}
                onLeftIconButtonTouchTap={this.activateUserBar}
                iconClassNameRight="material-icons icon-navigation-expand-more"/>
        <Toolbar className={this.state.add ? 'expanded': 'collapsed'} style={{backgroundColor:'white'}}>
          <SearchPanel/>
        </Toolbar>
        <TabsPanel/>
        <LeftNav docked={false} width={320} open={this.state.open} onRequestChange={open => this.setState({open})}>          
          <AccountPanel />
        </LeftNav>
      </div>
    );
  }

  activateUserBar() {
    this.setState({open: !this.state.open});
  }

  activateAddBar() {
    this.setState({add: !this.state.add});
  }

  tabChange(value) {
    this.setState({tabIndex: value});
  }

  handleClose() {
    this.setState({open: false, add: false});
  }
}
