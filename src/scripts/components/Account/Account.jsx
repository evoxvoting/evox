import React from 'react';
import AltContainer from 'alt-container';

import ProfilePanel from './Profile.jsx';
import RegisterPanel from './Register.jsx';
import LoginPanel from './Login.jsx';

import AccountPanelActions from './../../actions/panel.actions.jsx';
import AccountPanelStore from './../../stores/panel.store.jsx';
import types from '../../types.jsx';

const componentsMap = {
  [types.LOGIN]: <LoginPanel/>,
  [types.REGISTRATION]: <RegisterPanel/>,
  [types.PROFILE]: <ProfilePanel/>
};

class Account extends React.Component {
  render() {
    return componentsMap[this.props.panelType];
  }
}

export default class AccountPanel extends React.Component {
  render() {
    return (
      <AltContainer store={AccountPanelStore} actions={AccountPanelActions}>
        <Account/>
      </AltContainer>
    );
  }
}
