import React from 'react';
import AltContainer from 'alt-container';

import AccountStore from '../../stores/account.store.jsx';
import PanelStore from '../../stores/panel.store.jsx';

import AccountActions from '../../actions/account.actions.jsx';
import PanelActions from '../../actions/panel.actions.jsx';

import Divider from 'material-ui/lib/divider';
import TextField from 'material-ui/lib/text-field';
import RaisedButton from 'material-ui/lib/raised-button';
import Dialog from 'material-ui/lib/dialog';
import AppBar from 'material-ui/lib/app-bar';
import Loading from './../Loading.jsx';

const css = {
  fields: {marginLeft: '2rem'},
  title: {marginLeft: '1rem', color: 'black'},
  item: {marginLeft: '1rem'},
  send: {marginTop: '1rem', marginLeft: '2rem', marginBottom: '2rem'},
  back: {marginTop: '1rem', marginLeft: '3rem'}
};


const txt = {
  name: 'Ім\'я',
  surname: 'Прізвище',
  email: 'Email',
  pass: 'Пароль',
  send: 'Надіслати',
  back: 'Назад',
  title: 'Реєстрація'
};


class Register extends React.Component {
  parseError(errors) {
    return JSON.stringify(errors && errors.data && errors.data.message);
  }

  render() {
    const hasError = !!this.props.AccountStore.errors;
    const errors = this.parseError(this.props.AccountStore.errors);
    const PanelActions = this.props.PanelActions;
    const user = this.props.PanelStore.user;
    const Close = [<RaisedButton label="Close" primary={true} onTouchTap={this.props.AccountStore.errorClear}/>];
    const loading = this.props.PanelStore.loading;

    return (
      <div>
        <AppBar title={txt.title} showMenuIconButton={false}/>
        <Divider />
        <TextField onChange={PanelActions.setName} value={user.name} hintText={txt.name} floatingLabelText={txt.name} style={css.fields} type="text"/>
        <TextField onChange={PanelActions.setSurname} value={user.surname} hintText={txt.surname} floatingLabelText={txt.surname} style={css.fields} type="text"/>
        <TextField onChange={PanelActions.setEmail} value={user.email} hintText={txt.email} floatingLabelText={txt.email} style={css.fields} type="email"/>
        <TextField onChange={PanelActions.setPassword} value={user.password} hintText={txt.pass} floatingLabelText={txt.pass} style={css.fields} type="password"/>
        <Loading open={loading}/>
        <Dialog title="Invalid user data" actions={Close} open={hasError}>{errors}</Dialog>
        <RaisedButton onTouchTap={() => this.props.AccountActions.createAccount(user)} primary={true} label={txt.send} style={css.send}/>
        <RaisedButton onTouchTap={PanelActions.showLogin} label={txt.back} style={css.back}/>
      </div>
    );
  }
}

export default class RegisterPanel extends React.Component {
  render() {
    return (
      <AltContainer stores={{AccountStore, PanelStore}} actions={{AccountActions,PanelActions}}>
        <Register/>
      </AltContainer>
    )
  }
}
