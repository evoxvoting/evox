import React from 'react';
import AltContainer from 'alt-container';

import AccountStore from '../../stores/account.store.jsx';
import PanelStore from '../../stores/panel.store.jsx';

import AccountActions from '../../actions/account.actions.jsx';
import PanelActions from '../../actions/panel.actions.jsx';


import MenuItem from 'material-ui/lib/menus/menu-item';
import Divider from 'material-ui/lib/divider';
import RaisedButton from 'material-ui/lib/raised-button';
import PetitionCreatorDialog from './../PetitionsCreator/PetitionCreator.jsx';
import AppBar from 'material-ui/lib/app-bar';

const css = {
  title: {marginLeft: '1rem', color: 'black'},
  item: {marginLeft: '1rem'},
  logout: {margin: '1rem 1rem 2rem 2rem'},
  token: {marginLeft: '1rem', fontSize: '0.7rem'}
};

const txt = {
  logout: 'Вихід',
  title: 'Профіль'
};

export default class Profile extends React.Component {
  render() {
    const user = this.props.PanelStore.user;
    const token = user.token;
    const url = `https://shapeshift.io/shifty.html?destination=${token}&output=ETH`;
    return (
      <div>
        <AppBar title={txt.title} showMenuIconButton={false}/>
        <Divider />
        <MenuItem style={css.item} disabled={true} primaryText={user.name}/>
        <MenuItem style={css.item} disabled={true} primaryText={user.surname}/>
        <MenuItem style={css.item} disabled={true} primaryText={user.email}/>
        <MenuItem style={css.token} disabled={true} primaryText={user.token}/>
        <MenuItem style={css.item} disabled={true} primaryText={'Баланс: ' + user.balance + ' ETH'}/>
        <MenuItem style={css.item} disabled={true}>
          <a href={url}>{'Поповнити баланс'}</a>
        </MenuItem>
        <RaisedButton onTouchTap={this.props.PanelActions.logout} primary={true} label={txt.logout} style={css.logout}/>
        <Divider />
        <PetitionCreatorDialog/>
      </div>
    );
  }
}

export default class ProfilePanel extends React.Component {
  render() {
    return (
      <AltContainer stores={{AccountStore, PanelStore}} actions={{AccountActions,PanelActions}}>
        <Profile/>
      </AltContainer>
    )
  }
}
