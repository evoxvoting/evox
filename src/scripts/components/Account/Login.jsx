import React from 'react';
import AltContainer from 'alt-container';

import AccountStore from '../../stores/account.store.jsx';
import PanelStore from '../../stores/panel.store.jsx';

import AccountActions from '../../actions/account.actions.jsx';
import PanelActions from '../../actions/panel.actions.jsx';
import AppBar from 'material-ui/lib/app-bar';

import Divider from 'material-ui/lib/divider';
import TextField from 'material-ui/lib/text-field';
import RaisedButton from 'material-ui/lib/raised-button';
import Dialog from 'material-ui/lib/dialog';
import Loading from './../Loading.jsx';

const css = {
  fields: {marginLeft: '2rem'},
  title: {marginLeft: '1rem', color: 'black'},
  item: {marginLeft: '1rem'},
  enter: {marginTop: '1rem', marginLeft: '2rem', marginBottom: '2rem'},
  register: {marginTop: '1rem', marginLeft: '3rem'}
};

const txt = {
  email: 'Email',
  pass: 'Пароль',
  enter: 'Вхід',
  register: 'Реєстрація',
  title: 'Вхiд'
};


class Login extends React.Component {
  parseError(errors) {
    return JSON.stringify(errors && errors.data && errors.data.message);
  }

  render() {
    const hasError = !!this.props.AccountStore.errors;
    const errors = this.parseError(this.props.AccountStore.errors);
    const user = this.props.PanelStore.user;
    const loading = this.props.PanelStore.loading;
    const Close = [<RaisedButton label="Close" primary={true} onTouchTap={this.props.AccountActions.errorClear}/>];

    return (
      <div>
        <AppBar title={txt.title} showMenuIconButton={false}/>
        <Divider />
        <TextField onChange={this.props.PanelActions.setEmail} value={user.email} hintText={txt.email} floatingLabelText={txt.email} style={css.fields} type="email"/>
        <TextField onChange={this.props.PanelActions.setPassword} value={user.password} hintText={txt.pass} floatingLabelText={txt.pass} style={css.fields} type="password"/>
        <Loading open={loading}/>
        <Dialog title="Account not found" actions={Close} open={hasError}>{errors}</Dialog>
        <RaisedButton onTouchTap={() => this.props.AccountActions.login(user)} primary={true} label={txt.enter} style={css.enter}/>
        <RaisedButton onTouchTap={this.props.PanelActions.showRegister} label={txt.register} style={css.register}/>
      </div>
    );
  }
}


export default class LoginPanel extends React.Component {
  render() {
    return (
      <AltContainer stores={{AccountStore, PanelStore}} actions={{AccountActions,PanelActions}}>
        <Login/>
      </AltContainer>
    )
  }
}
