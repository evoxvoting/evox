import React from 'react';
import AltContainer from 'alt-container';
import PetitionsStore from './../../stores/petitions.store.jsx'
import PetitionActions from './../../actions/petition.actions.jsx'

import ToolbarGroup from 'material-ui/lib/toolbar/toolbar-group';
import TextField from 'material-ui/lib/text-field';
import RaisedButton from 'material-ui/lib/raised-button';
import FontIcon from 'material-ui/lib/font-icon';

class Search extends React.Component {
  constructor(props, context) {
    super(props, context);
  }

  render() {
    return (
      <ToolbarGroup style={{width:"100%", float:"none", textAlign:"center"}}>
        <TextField onChange={this.props.setFilter} hintText="Назва петиції" style={{width: '49%', float:"none", textAlign:"center"}}/>
        <RaisedButton primary={true} onTouchTap={this.props.doFilter} label="Пошук" style={{float:'none'}} icon={<FontIcon className="material-icons icon-search"/>}/>
      </ToolbarGroup>
    );
  }
}


export default class SearchPanel extends React.Component {
  constructor(props, context) {
    super(props, context);
  }

  render() {
    return (
      <AltContainer store={PetitionsStore} actions={PetitionActions}>
        <Search/>
      </AltContainer>
    );
  }
}
