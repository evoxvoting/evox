import React from 'react';
import RefreshIndicator from 'material-ui/lib/refresh-indicator';
import Dialog from 'material-ui/lib/dialog';


export default class Loading extends React.Component {
  render() {
    return (
      <Dialog open={this.props.open} modal={true} contentStyle={{width:'300px'}}>
        <div>{'Триває запит, зачекайте...'}</div>
        <RefreshIndicator size={40} left={240} top={12} status="loading"/>
      </Dialog>
    );
  }
}
