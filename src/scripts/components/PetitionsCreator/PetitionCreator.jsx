import React from 'react';
import RaisedButton from 'material-ui/lib/raised-button';
import TextField from 'material-ui/lib/text-field';
import DatePicker from 'material-ui/lib/date-picker/date-picker';
import Dialog from 'material-ui/lib/dialog';
import PetitionsStore from './../../stores/petitions.store.jsx'
import PetitionActions from './../../actions/petition.actions.jsx'
import AltContainer from 'alt-container';


const css = {
  petition: {minWidth: '256px', marginTop: '2rem', marginLeft: '2rem', marginBottom: '0rem'}
};

class PetitionsCreator extends React.Component {
  parseError(errors) {
    return JSON.stringify(errors && errors.data && errors.data.message);
  }

  render() {
    const Actions = [
      <RaisedButton label="Скасувати" onTouchTap={this.props.handleClose} style={{margin:"0rem 1rem"}}/>,
      <RaisedButton label="Створити" onTouchTap={this.props.makePetition} primary={true} style={{margin:"0rem 1rem"}}/>
    ];
    let hasError = !!this.props.fundsErrors;

    let errors = this.parseError(this.props.fundsErrors);
    const Close = [<RaisedButton label="Гаразд" primary={true} onTouchTap={() => {hasError = {} }}/>];
     return (
      <div>
        <Dialog title="Нова петиція" modal={true} actions={Actions} open={this.props.open}>
          <TextField fullWidth={true} hintText="Назва петиції" floatingLabelText="Назва петиції" value={this.props.petition.title} onChange={this.props.setTitle}/>
          <TextField fullWidth={true} hintText="Опис петиції" floatingLabelText="Опис петиції" multiLine={true} rows={2} rowsMax={10} value={this.props.petition.text} onChange={this.props.setText}/>
          <DatePicker hintText="Старт збору підписів" onChange={this.props.setStart} wordings={{ok:'Вибрати', cancel: 'Скасувати'}}/>
          <DatePicker hintText="Кінець збору підписів" onChange={this.props.setEnd} wordings={{ok:'Вибрати', cancel: 'Скасувати'}}/>
        </Dialog>
        <Dialog modal={true} title="Недостатньо коштiв для виконання запиту" actions={Close} open={hasError}>{errors}</Dialog>
        <RaisedButton fullWidth={true} label="Створити петицію" onTouchTap={this.props.handleOpen} secondary={true} style={css.petition}/>
        <RaisedButton fullWidth={true} label="Поповнити +0.05 ETH" onTouchTap={this.props.sendMoney} secondary={true} style={css.petition}/>
      </div>
    )
  }
}

export default class PetitionCreatorDialog extends React.Component {
  render() {
    return (
      <AltContainer store={PetitionsStore} actions={PetitionActions}>
        <PetitionsCreator />
      </AltContainer>
    );
  }
}
