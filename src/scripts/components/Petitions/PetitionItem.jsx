import React from 'react';
import Voted from 'material-ui/lib/svg-icons/action/done';
import Empty from 'material-ui/lib/svg-icons/content/add';
import FloatingActionButton from 'material-ui/lib/floating-action-button';
import TableRow from 'material-ui/lib/table/table-row';
import TableRowColumn from 'material-ui/lib/table/table-row-column';
import Dialog from 'material-ui/lib/dialog';
import RaisedButton from 'material-ui/lib/raised-button';
import alt from './../../alt.jsx';
import types from './../../types.jsx';



export default class PetitionItem extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      opened: false
    };
  }

  render() {
    const isLoggedIn = alt.getStore('PanelStore').getState().panelType === types.PROFILE;
    const item = this.props.item;
   
    const Actions = [
      isLoggedIn && !item.voted ? <RaisedButton label="Пiдтримати" onTouchTap={()=>{this.props.vote(item.address); this.close()}} primary={true} style={{marginRight:'20px'}}/> : '',
      <RaisedButton label="Назад" onTouchTap={()=>this.close()} secondary={true}/>
    ];
    return (
      <TableRow className={"rowHovered"} displayRowCheckbox={false} onTouchTap={()=>this.open()}>

        <TableRowColumn style={{textAlign:'center'}}>{item.name}</TableRowColumn>
        <TableRowColumn style={{textAlign:'center', fontSize:'12px'}}>{item.address}</TableRowColumn>
        <TableRowColumn style={{textAlign:'center'}}>
          {item.daysLeft || '-'}
        </TableRowColumn>
        <TableRowColumn style={{textAlign:'center'}}>
          <FloatingActionButton mini={true} disabled={true}>
            {item.count}
            <Dialog title={item.name} modal={true} actions={Actions} open={this.state.opened}>
              {item.questions}
            </Dialog>
          </FloatingActionButton>
        </TableRowColumn>
      </TableRow>
    );
  }

  open() {
    this.setState({opened: true});
  }

  close() {
    this.setState({opened: false});
  }
}

/*
 *
 * <FloatingActionButton style={{marginLeft:'2rem'}} mini={true} hash={item.address} onTouchTap={() =>{
 this.props.vote(item.address)
 }}>
 {this.props.voted[item.address] ? <Voted/> : <Empty/>}   </FloatingActionButton>
 * */
