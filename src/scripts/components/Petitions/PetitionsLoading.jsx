import React from 'react';
import CircularProgress from 'material-ui/lib/circular-progress';
import Table from 'material-ui/lib/table/table';
import TableHeaderColumn from 'material-ui/lib/table/table-header-column';
import TableRow from 'material-ui/lib/table/table-row';
import TableRowColumn from 'material-ui/lib/table/table-row-column';
import TableHeader from 'material-ui/lib/table/table-header';
import TableBody from 'material-ui/lib/table/table-body';

export default class PetitionsLoading extends React.Component {
  render() {
    return (
      <Table>
        <TableHeader adjustForCheckbox={false} displaySelectAll={false} enableSelectAll={false}>
          <TableRow>
            <TableHeaderColumn style={{textAlign:'center'}}>{'Завантаження списку петицій...'}</TableHeaderColumn>
          </TableRow>
        </TableHeader>
        <TableBody displayRowCheckbox={false}>
          <TableRow key={-1}>
            <TableRowColumn style={{textAlign:'center'}} children={<CircularProgress innerStyle={{display:'inline-block'}} />}/>
          </TableRow>
        </TableBody>
      </Table>
    );
  }
}
