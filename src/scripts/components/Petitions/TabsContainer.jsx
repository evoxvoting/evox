import AltContainer from 'alt-container';
import TabsComponent from './TabsComponent.jsx';
import PetitionsStore from './../../stores/petitions.store.jsx'
import PetitionActions from './../../actions/petition.actions.jsx'


import React from 'react';

export default class TabsContainer extends React.Component {
  constructor(props, context) {
    super(props, context);
  }

  render() {
    return (
      <AltContainer store={PetitionsStore} actions={PetitionActions}>
        <TabsComponent/>
      </AltContainer>
    );
  }
}
