import React from 'react';
import Table from 'material-ui/lib/table/table';
import TableHeaderColumn from 'material-ui/lib/table/table-header-column';
import TableRow from 'material-ui/lib/table/table-row';
import TableHeader from 'material-ui/lib/table/table-header';
import TableBody from 'material-ui/lib/table/table-body';
import PetitionsLoading from './PetitionsLoading.jsx';
import PetitionItem from './PetitionItem.jsx';

export default class PetitionsTable extends React.Component {
  constructor(props, context) {
    super(props, context);
  }

  render() {
    const petitions = this.props.petitions || [];
    const isLoaded = petitions.length > 0;
    switch (this.props.type) {
      case 0:
        break;
      case 1:
        break;
      case 2:
        break;
      case 3:
        break;
    }
    return isLoaded ? <Table selectable={true} multiSelectable={true} onRowSelection={this.props.vote}>
      <TableHeader adjustForCheckbox={false} displaySelectAll={false} enableSelectAll={false}>
        <TableRow>
          <TableHeaderColumn style={{textAlign:'center'}}>Суть звернення</TableHeaderColumn>
          <TableHeaderColumn style={{textAlign:'center'}}>Номер петиції</TableHeaderColumn>
          <TableHeaderColumn style={{textAlign:'center'}}>Залишилося днів</TableHeaderColumn>
          <TableHeaderColumn style={{textAlign:'center'}}>Зібрано підписів</TableHeaderColumn>
        </TableRow>
      </TableHeader>
      <TableBody showRowHover={true} displayRowCheckbox={true}>
        {petitions.map((petition, index) => <PetitionItem vote={this.props.vote} voted={this.props.voted} key={petition.index.toString()} item={petition}/>)}
      </TableBody>
    </Table> : <PetitionsLoading/>
  }
}
