import React from 'react';
import PetitionsTable from './PetitionsTable.jsx';
import Tabs from 'material-ui/lib/tabs/tabs';
import Tab from 'material-ui/lib/tabs/tab';
import Loading from './../Loading.jsx';

const styles = {
  tab: {
    fontSize: "75%"
  }
};

export default class TabsBody extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.handleChange = this.handleChange.bind(this);
    this.state = {
      value: 0
    };
    this.props.startFetchingPetitions();
  }

  render() {
    const loading = this.props.loading;
    return (
      <div>
        <Loading open={loading}/>
        <Tabs value={this.state.value} onChange={(value) => {this.handleChange(); this.props.reducingBy(value)}}>
          <Tab style={styles.tab} label="Всі петиції" value={0}/>
          <Tab style={styles.tab} label="Збір підписів" value={1}/>
          <Tab style={styles.tab} label="На розгляді" value={2}/>
        </Tabs>
        <PetitionsTable vote={this.props.vote} voted={this.props.voted} petitions={this.props.petitions} type={this.state.value}/>
      </div>
    );
  }

  handleChange(value) {
    this.setState({
      value: value
    });
  };
}
