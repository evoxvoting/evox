import alt from '../alt.jsx';
import PanelActions from '../actions/panel.actions.jsx';
import types from '../types.jsx';
import EToken from '../etoken-lib/provider.jsx';

class PanelStore {
  constructor() {
    this.panelType = types.LOGIN;
    this.resetProps();
    this.bindActions(PanelActions);
  }

  startLoading() {
    this.loading = true;
    this.emitChange();
  }

  stopLoading() {
    this.loading = false;
    this.emitChange();
  }

  setName(event) {
    this.user.name = event.target.value;
  }

  setSurname(event) {
    this.user.surname = event.target.value;
  }

  setEmail(event) {
    this.user.email = event.target.value;
  }

  setPassword(event) {
    this.user.password = event.target.value;
  }

  resetProps() {
    this.user = {
      email: 'admin@admin',
      password: 'admin',
      balance: 0
    };
    this.loading = false;
  }

  setProfile(user) {
    this.user = user;
    EToken.web3.eth.getBalance(user.token, (err, balance) => {
      this.user.balance = (balance.toNumber() / Math.pow(10, 18)).toFixed(5);
      this.emitChange();
    });
    this.alt.actions.global.updateVotePermissions.defer();
  }

  gotBalance(balance, b2, b3) {
    //console.log(balance,b2,b3)
    //this.user.balance = balance.data
    //this.emitChange();
  }

  logout() {
    this.user = {};
    this.panelType = types.LOGIN;
    this.emitChange();
  }

  showRegister() {
    this.panelType = types.REGISTRATION;
    this.resetProps();
    this.emitChange();
  }

  showLogin() {
    this.panelType = types.LOGIN;
    this.resetProps();
    this.emitChange();
  }

  showProfile() {
    this.panelType = types.PROFILE;
    this.emitChange();
  }
}

export default alt.createStore(PanelStore, 'PanelStore');
