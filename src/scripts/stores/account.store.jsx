import alt from '../alt.jsx';
import AccountSource from '../sources/account.source.jsx';
import EthSource from '../sources/eth.source.jsx';
import AccountActions from '../actions/account.actions.jsx';

class AccountStore {
  constructor() {
    this.auth = '';
    this.errors = false;
    this.bindActions(AccountActions);
    this.registerAsync(AccountSource);
    this.registerAsync(EthSource);
  }

  login(user) {
    this.auth = '';
    this.errors = false;
    this.alt.actions.global.startLoading.defer();
    this.getInstance().performLogin(user);
  }

  loggedIn(response) {
    this.getInstance().getProfile(response.data.token);
  }

  gotProfile(response) {
    const PanelStore = this.alt.getStore('PanelStore');
    const user = Object.assign(PanelStore.getState().user, response.data);
    this.alt.actions.global.setProfile.defer(user);
    this.getInstance().validateAccount(user);
  }

  validated() {
    this.alt.actions.global.stopLoading.defer();
    this.alt.actions.global.showProfile.defer();

    const PanelStore = this.alt.getStore('PanelStore');
    this.getInstance().getBalance(PanelStore.getState().user);
  }

  /************/

  createAccount(user) {
    this.alt.actions.global.startLoading.defer();
    this.getInstance().createEthAccount(user);
  }

  created(token) {
    const PanelStore = this.alt.getStore('PanelStore');
    const user = PanelStore.getState().user;
    user.token = token;
    this.getInstance().saveCredentials(user);
  }

  saved(response) {
    this.loggedIn(response);
  }

  errorClear() {
    this.errors = false;
    this.emitChange();
  }

  fail(response) {
    this.alt.actions.global.stopLoading.defer();
    this.errors = response;
    this.auth = '';
    this.emitChange();
//    this.alt.actions.global.resetProps.defer();
  }
}

export default alt.createStore(AccountStore, 'AccountStore');
