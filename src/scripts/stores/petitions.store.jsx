import alt from '../alt.jsx';
import EthSource from '../sources/eth.source.jsx';
import PetitionActions from '../actions/petition.actions.jsx';
import moment from 'moment';


import EToken from '../sources/etoken-lib.abstraction.jsx';
let orginal = [];
class PetitionsStore {
  constructor() {
    this.petitions = [];
    /*
     * "{"startTime":"1453174257","endTime":"1454174257","count":"5","index":"0xfa8826004fae76d989acbbb2ba06baf23b2dbcea","name":"Crippled Evening"}"
     * */
    this.petition = {
      title: '',
      text: '',
      start: new Date().getTime(),
      end: new Date().getTime()
    };
    this.voted = {};
    this.loading = false;
    this.open = false;
    this.filter = '';
    this.bindActions(PetitionActions);
    this.registerAsync(EthSource);
  }

  updateVotePermissions() {
    const user = this.alt.getStore('PanelStore').getState().user;
    this.petitions.forEach((petition) => {
      petition.pollInstance.index(user.token, (error, voted)=> {
        petition.voted = voted > 0;
      })
    });
    this.emitChange();
  }

  errorClear() {
    this.fundsErrors = undefined;
    this.emitChange();
  }

  handleOpen() {
    this.open = true;
    this.emitChange();
  }

  handleClose() {
    this.open = false;
    this.emitChange();
  }

  doFilter() {
    this.petitions = orginal.filter(petition => petition.name.includes(this.filter));
    this.emitChange();
  }

  setFilter(event) {
    this.filter = event.target.value;
  }

  reducingBy(type) {
    switch (type) {
      case 1:
        this.petitions = orginal.filter((item) => ( new Date().getTime()) <= item.endTime);
        break;
      case 2:
        this.petitions = orginal.filter((item) =>( new Date().getTime()) > item.endTime);
        break;
      default:
        this.petitions = orginal;
        break;
    }
    this.emitChange();
  }

  startFetchingPetitions() {
    const user = this.alt.getStore('PanelStore').getState().user;
    EToken.receivePollList(petition => {
      const diff = moment(petition.endTime).diff(moment(petition.startTime));
      petition.daysLeft = moment.duration(diff).days();
      petition.pollInstance.index(user.token, (error, voted)=> {
        petition.voted = voted > 0;
        this.petitions.push(petition);
        orginal = this.petitions;
        this.emitChange();
      });
    });
  }

  makePetition() {
    const PanelStore = this.alt.getStore('PanelStore');
    const user = PanelStore.getState().user;

    if (!this.getInstance().isLoading()) {
      this.loading = true;
      this.fundsErrors = '';
      this.open = false;
      this.emitChange();
      this.getInstance().createPoll(user.token, user.password);
    }
  }

  sendMoney() {
    const PanelStore = this.alt.getStore('PanelStore');
    const token = PanelStore.getState().user.token;
    const money = '50000000000000000';

    if (!this.getInstance().isLoading()) {
      this.loading = true;
      this.emitChange();
      this.getInstance().sendExternalFunds(token, money);
    }
  }

  sendDone(response) {
    this.loading = false;
    this.emitChange();
  }

  sendFail(response) {
    this.loading = false;
    this.fundsErrors = response;

    console.log(response);
    //if (response) {
      this.fundsErrors = response;
    //}
    this.emitChange();
  }

  vote(pollAddress) {
    if (!this.voted[pollAddress]) {
      this.loading = true;
      const PanelStore = this.alt.getStore('PanelStore');
      const user = PanelStore.getState().user;
      if (!this.getInstance().isLoading()) {
        this.getInstance().voteForPoll(user.token, user.password, pollAddress);
      }
    }

    this.voted[pollAddress] = true;
    this.emitChange();
  }

  voteDone(response) {
    console.log(response);
    const voted = this.petitions.find((item) => item.address === response.user.address);
    voted.count++;
    this.loading = false;
    this.emitChange();
  }

  voteFail(response) {
    console.log(response);

    this.fundsErrors = response;

    this.loading = false;
    this.emitChange();
  }

  setTitle(event) {
    this.petition.title = event.target.value;
  }

  setText(event) {
    this.petition.text = event.target.value;
  }

  setStart(date) {
    this.petition.start = date[1].getTime();
  }

  setEnd(date) {
    this.petition.end = date[1].getTime();
  }
}
/*

 orginal.map(function(t){
 var a = new Date();
 a.setTime(t.startTime.toNumber());
 return a;
 }).forEach(function(d){
 console.log(d)
 })

 */

export default alt.createStore(PetitionsStore, 'PetitionsStore');
