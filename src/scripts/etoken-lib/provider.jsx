'use strict';

import AccountStorage from 'account-storage';
import Ambisafe from 'ambisafe-client-javascript';
import Web3Subprovider from 'web3-provider-engine/subproviders/web3'
import HookedWalletEthTxSubprovider from 'web3-provider-engine/subproviders/hooked-wallet-ethtx'

import engine from './engine.jsx';
import web3 from './web3.jsx';
import storage from './storage.jsx';

import {waitForTransaction, publicToAddress, privateToAddress} from './helpers.jsx';

let signerPrivateKey, signerAddress;

engine.addProvider(new HookedWalletEthTxSubprovider({
  getPrivateKey: function (address, callback) {
    if (address == signerAddress) {
      callback(null, signerPrivateKey);
    } else {
      storage.getPrivateKey(address, callback);
    }
  }
}));

storage.web3 = web3;

function createAccount(password, callback) {
  const container = Ambisafe.generateAccount('ETH', password);
  const serializedContainer = container.getContainer();
  const address = publicToAddress(container.get('public_key'), true);
  if (!signerAddress) {
    throw Error('You must specify private key first');
  }
  storage.accountSaverAddress = signerAddress;
  storage.addAccount(address, serializedContainer, (err, result) => {
    if (err) {
      callback(err);
    } else {
      callback(null, {address: address, transactionHash: result})
    }
  });
}


function setPassword(password) {
  storage.password = password;
}


function setPrivateKey(privateKey) {
  signerPrivateKey = new Buffer(privateKey, "hex");
  signerAddress = privateToAddress(signerPrivateKey);
}

function start(config) {
  engine.addProvider(new Web3Subprovider(new web3.providers.HttpProvider(config.node)));
  engine.start();
}

export default {
  start,
  web3,
  Ambisafe,
  AccountStorage,
  storage,
  publicToAddress,
  privateToAddress,
  waitForTransaction,
  createAccount,
  setPassword,
  setPrivateKey
};
