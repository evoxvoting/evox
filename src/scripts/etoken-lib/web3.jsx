import Web3 from 'web3';
import engine from './engine.jsx';

export default new Web3(engine);
