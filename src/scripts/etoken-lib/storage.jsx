import AccountStorage from 'account-storage';
import web3 from './web3.jsx';

export default new AccountStorage('0x152c21d6944f32c6b45605af12bb9b7231a456e7', web3);
