'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var PetitionSchema = new Schema({
  startTime: Number,
  endTime: Number,
  count: Number,
  index: String,
  questions: String,
  name: String
});
 

UserSchema.methods = {};

module.exports = mongoose.model('Petition', UserSchema);
