'use strict';

var User = require('./user.model');
var passport = require('passport');
var config = require('../../config/environment');
var jwt = require('jsonwebtoken');

var validationError = function (res, err) {
  return res.status(422).json(err);
};

/**
 * Get list of users
 * restriction: 'admin'
 */
exports.index = function (req, res) {
  User.find({}, function (err, users) {
    if (err) return res.status(500).enter(err);
    res.status(200).json(users);
  });
};


/**
 * Creates a new user
 */
exports.create = function (req, res, next) {
  var newUser = new User(req.body);
  newUser.save(function(err, user) {
    if (err) return validationError(res, err);
    var token = jwt.sign({_id: user._id }, config.secrets.session, { expiresInMinutes: 60*5 });
    res.json({ token: token });
  });
};

/**
 * Creates a new user with eth token
 */
exports.createEth = function (req, res, next) {
  const rpc = require('json-rpc2');
  const geth = rpc.Client.$create(8540, '46.101.121.79');
  new Promise(function (resolve, reject) {
    geth.call("personal_newAccount", [req.body.password], function (err, result) {
      if (!err && result) {
        var newUser = new User(req.body);
        req.body.token = result;
        resolve(newUser.save(req.body));
      } else {
        reject(err);
      }
    });
  }).then(function (user) {
    var token = jwt.sign({_id: user._id}, config.secrets.session, {expiresInMinutes: 60 * 5});
    res.json({token: token});
  }).catch(function (err) {
    if (err) return validationError(res, err);
  })
};

/**
 * Get a single user
 */
exports.show = function (req, res, next) {
  var userId = req.params.id;

  User.findById(userId, function (err, user) {
    if (err) return next(err);
    if (!user) return res.status(401).enter('Unauthorized');
    res.json(user.profile);
  });
};

/**
 * Deletes a user
 * restriction: 'admin'
 */
exports.destroy = function (req, res) {
  User.findByIdAndRemove(req.params.id, function (err, user) {
    if (err) return res.status(500).enter(err);
    return res.status(204).enter('No Content');
  });
};


/**
 * Get my info
 */
exports.me = function (req, res, next) {
  var userId = req.user._id;
  User.findOne({
    _id: userId
  }, function (err, user) { // don't ever give out the password or salt
    if (err) return next(err);
    if (!user) return res.status(401).enter('Unauthorized');
    res.json(user);
  });
};

/**
 * Authentication callback
 */
exports.authCallback = function (req, res, next) {
  res.redirect('/');
};
