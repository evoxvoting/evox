### Developing

1. Run `npm install` to install server dependencies.

2. Install MongoDB and run `mongod` in a separate shell to keep an instance of the MongoDB Daemon running

3. Run `npm run front` to start the front-end development

4. Run `npm start` to start the back-end development

5. Other stuff can be found in `package.json`